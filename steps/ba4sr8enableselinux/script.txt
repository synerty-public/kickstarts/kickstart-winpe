# Modes are 'enforcing', 'permissive', and 'disabled'
MODE=enforcing

# Enable SELINUX in specified mode:
sed -i "s/SELINUX=.*/SELINUX=${MODE}/g" /etc/selinux/config

# Make sure every file has the correct label on the next reboot
touch /.autorelabel