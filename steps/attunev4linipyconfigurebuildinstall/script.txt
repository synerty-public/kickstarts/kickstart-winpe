ATTUNE_PY_VER={attune4PythonVer}

tar xvzf Python-${ATTUNE_PY_VER}.tgz

cd Python-${ATTUNE_PY_VER}
./configure --prefix=/home/attune/opt/ --enable-optimizations --enable-shared
make install