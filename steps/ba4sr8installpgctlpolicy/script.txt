NAME=pg-ctl

mkdir -pv selinux
cd selinux
rm -rfv ${NAME} || true
mkdir -v ${NAME}
cd ${NAME}

echo "SELinux policy modules created by Attune build blueprint" > README.txt

echo "Creating ${NAME}.te"
cat <<EOF | tee ${NAME}.te

module ${NAME} 1.0;

require {
        type init_t;
        type user_home_t;
        class file { execute execute_no_trans map open read };
}

#============= init_t ==============

#!!!! This avc can be allowed using the boolean 'domain_can_mmap_files'
allow init_t user_home_t:file map;
allow init_t user_home_t:file { execute execute_no_trans open read };
EOF

checkmodule -M -m -o ${NAME}.mod ${NAME}.te

echo "Building ${NAME}.pp"
semodule_package -o ${NAME}.pp -m ${NAME}.mod

echo "Installing SELinux policy module ${NAME}.pp"
semodule -i ${NAME}.pp