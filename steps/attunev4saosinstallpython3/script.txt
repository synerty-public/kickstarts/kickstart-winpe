yum install -y $(cat <<EOF
    gcc
    nss
    libxml2-devel
    python3-devel
    python3
    python3-virtualenv
EOF
)