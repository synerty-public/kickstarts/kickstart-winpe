cd attune.home
F=config.cfg
CERT_PATH=/home/attune/attune.home/certs

# This function checks whether a line beginning with SETTING exists
# and creates or updates this line accordingly.
updateOrAdd() {
    SETTING="$1"
    FIND="^${SETTING}.*"
    REPLACE="$2"
    
    if grep -q "${FIND}" ${F}
    then
        echo "Updating ${SETTING}"
        sed -i "s,${FIND},${REPLACE},g" ${F}
    else
        echo "Adding ${SETTING}"
        echo "${REPLACE}" >> ${F}
    fi
}

updateOrAdd key_file_path "key_file_path = ${CERT_PATH}/attune.key"
updateOrAdd cert_file_path "cert_file_path = ${CERT_PATH}/attune.cer"
updateOrAdd https_port "https_port = 8001"

echo "Resulting configuration is "
grep -A 10 http $F | grep -v 'pass\|url'