setbool() {
    BOOL=$1
    REASON=$2
    echo "Setting ${BOOL} to ${REASON}"
    
    # Set the boolean
    setsebool -P ${BOOL} 1
    
    # Print the result
    getsebool ${BOOL}
}

setbool rsync_full_access "allow rsync to access all directories"
setbool nis_enabled "allow Attune to create outbound SSH connections"