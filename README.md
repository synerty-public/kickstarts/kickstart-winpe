



[![Docs](https://img.shields.io/badge/docs-latest-brightgreen.svg)](http://doc.servertribe.com)
[![Discord](https://img.shields.io/discord/844971127703994369)](http://discord.servertribe.com)
[![Docs](https://img.shields.io/badge/videos-watch-brightgreen.svg)](https://www.youtube.com/@servertribe)
[![Generic badge](https://img.shields.io/badge/download-latest-brightgreen.svg)](https://www.servertribe.com/community-edition/)

# Kickstart Windows






# Attune

[Attune](https://www.servertribe.com/)
automates and orchestrates processes to streamline deployments, scaling,
migrations, and management of your systems. The Attune platform is building a
community of sharable automated and orchestrated processes.

You can leverage the publicly available orchestrated blueprints to increase
your productivity, and accelerate the delivery of your projects. You can
open-source your own work and improve existing community orchestrated projects.

## Get Started with Attune, Download NOW!

The **Attune Community Edition** can be
[downloaded](https://www.servertribe.com/comunity-edition/)
for free from our
[ServerTribe website](https://www.servertribe.com/comunity-edition/).
You can learn more about Attune through
[ServerTribe's YouTube Channel](https://www.youtube.com/@servertribe).







# Clone this Project

To clone this project into your own instance of Attune, follow the
[Clone a GIT Project How To Instructions](https://servertribe-attune.readthedocs.io/en/latest/howto/design_workspace/clone_project.html).




## Blueprints

This Project contains the following Blueprints.



### Attune v5 Setup WinPE Support


### Attune v5 Setup WinPE Support RHEL8


### KS Deploy Win10 Unattended Config - Parent


### KS WinPE Create Boot ISO - parent


### Modify WinPE boot.wim for 2016


### Modify WinPE boot.wim for 2019


### Modify WinPE boot.wim for 2022


### Modify WinPE boot.wim for Win10


### WinPE Deploy WinPE Files for ESXi


### KS Deploy Win2019 Unattended Config - Parent


### KS Extract WinPE ISO - Parent





## Parameters


| Name | Type | Script Reference | Comment |
| ---- | ---- | ---------------- | ------- |
| Active Directory Server | Windows Node | `activedirectoryserver` |  |
| AD Allowed Group | Text | `adallowedgroup` | The AD Domain Services group name that users must belong to, to gain access |
| AD Full Domain Name | Text | `adfulldomainname` |  |
| Attune Admin User | Basic Credential | `attuneadminuser` |  |
| Attune OS Build Server | Linux/Unix Node | `attuneosbuildserver` | This variable is used in the "Kickstart" build procedures, so the "Attune Server" can be used to build Attune servers. |
| Attune Server | Linux/Unix Node | `attuneserver` |  |
| Hosts File Servers | Node List | `hostsfileservers` |  |
| Kerberos Full Domain Name | Text | `kerberosfulldomainname` | This is the same as the AD Full Domain Name except that is lower case and this value is UPPER case. |
| Kickstart Organisation Name | Text | `kickstartorganisationname` |  |
| KS Linux: Disk First Letter | Text | `kslinuxdiskfirstletter` | The first letter of the disk in Linux, EG, sda or xda |
| KS: VM CPU Count | Text | `ksvmcpucount` |  |
| KS: VM Ram Size GB | Text | `ksvmramsizegb` |  |
| KS VMWare: Attune Base Dir | Text | `ksvmwareattunebasedir` |  |
| KS VMWare: Boot ISO Dir | Text | `ksvmwarebootisodir` |  |
| KS VMWare: Guest Type | Text | `ksvmwareguesttype` | https://vdc-download.vmware.com/vmwb-repository/dcr-public/8946c1b6-2861-4c12-a45f-f14ae0d3b1b9/a5b8094c-c222-4307-9399-3b606a04af55/vim.vm.GuestOsDescriptor.GuestOsIdentifier.html<br><br>https://vdc-download.vmware.com/vmwb-repository/dcr-public/da47f910-60ac-438b-8b9b-6122f4d14524/16b7274a-bf8b-4b4c-a05e-746f2aa93c8c/doc/vim.vm.GuestOsDescriptor.GuestOsIdentifier.html |
| KS VMWare: Network Name | Text | `ksvmwarenetworkname` |  |
| KS VMWare: Storage Pool Name | Text | `ksvmwarestoragepoolname` |  |
| KS: Windows Interface Alias | Text | `kswindowsinterfacealias` | oVirt Deployments = "Ethernet Instance 0"<br>ESXi Deployments = "Ethernet0"<br><br>This is the "InternetAlias" of the interface shown when you run "get-netipaddress" from powershell on the machine. |
| Linux: Attune User | Linux/Unix Credential | `linuxattuneuser` |  |
| Linux: Environment Name | Text | `linuxenvironmentname` |  |
| Linux: Prompt Color | Text | `linuxpromptcolor` |  |
| Linux: Root User | Linux/Unix Credential | `linuxrootuser` |  |
| NTP Servers | Node List | `ntpservers` |  |
| oVirt: Bios Type | Text | `ovirtbiostype` | Valid Values are (they must be in all capitals):<br>1. CLUSTER_DEFAULT - Use the cluster-wide default.<br>2. I440FX_SEA_BIOS - i440fx chipset with SeaBIOS.<br>3. Q35_OVMF - q35 chipset with OVMF (UEFI) BIOS.<br>4. Q35_SEA_BIOS - q35 chipset with SeaBIOS.<br>5. Q35_SECURE_BOOT- q35 chipset with OVMF (UEFI) BIOS with SecureBoot enabled.<br><br>https://ovirt.github.io/ovirt-engine-api-model/4.5/#types/bios_type |
| oVirt: Cluster Name | Text | `ovirtclustername` |  |
| oVirt: CPU Count | Text | `ovirtcpucount` |  |
| oVirt: Datacenter Name | Text | `ovirtdatacentername` |  |
| oVirt: Disk Interface | Text | `ovirtdiskinterface` | SATA or IDE required for Windows<br>VIRTIO_SCSI for windows after driver install<br>VIRTIO for Linux |
| oVirt: Disk Storage Name | Text | `ovirtdiskstoragename` |  |
| oVirt: Engine API User | Basic Credential | `ovirtengineapiuser` |  |
| oVirt: Engine Server | Basic Node | `ovirtengineserver` |  |
| oVirt: Host Server | Linux/Unix Node | `ovirthostserver` |  |
| oVirt: Host SSH User | Linux/Unix Credential | `ovirthostsshuser` |  |
| oVirt: ISO Storage Name | Text | `ovirtisostoragename` |  |
| oVirt: ISO Storage Path | Text | `ovirtisostoragepath` |  |
| oVirt: Memory Size | Text | `ovirtmemorysize` |  |
| oVirt: Network Name | Text | `ovirtnetworkname` |  |
| oVirt: NIC Interface | Text | `ovirtnicinterface` | E1000 for Windows<br>VIRTIO for Linux |
| oVirt: TimeZone | Text | `ovirttimezone` |  |
| Postgresql Service User | Basic Credential | `postgresqlserviceuser` |  |
| RPM Server | Linux/Unix Node | `rpmserver` |  |
| Samba Server IP Address | Text | `sambaserveripaddress` |  |
| Samba Windows Directory | Text | `sambawindowsdirectory` | The extracted Windows Directory on the Samba server that we want to run setup.exe from.<br>This could be "windows10", "windows2016" or "windows2019" for Windows operating system version 10, 2016 or 2019. |
| Smtp Server | Basic Node | `smtpserver` | This placeholder represents the SMTP smart host server where all mail will be sent to.<br>The SMTP smart host then sends the mail where it needs to go. |
| Target Server | Basic Node | `targetserver` |  |
| Target Server: Lin | Linux/Unix Node | `targetserverlin` | The target server is a generic placeholder, usually used for the server a script will run on.<br>For example, the server being built if the procedure is building a server. |
| Target Server: Linux Language | Text | `targetserverlinuxlanguage` |  |
| Target Server: Linux TimeZone | Text | `targetserverlinuxtimezone` |  |
| Target Server: Win | Windows Node | `targetserverwin` |  |
| Target Subnet | Network IPv4 Subnet | `targetsubnet` |  |
| VMWare: ESXi Server | Basic Node | `vmwareesxiserver` |  |
| VMWare: vCenter Server | Basic Node | `vmwarevcenterserver` |  |
| VMWare: vCenter User | Basic Credential | `vmwarevcenteruser` |  |
| Windows: AD Admin User | Windows Credential | `windowsadadminuser` |  |
| Windows: Administrator | Windows Credential | `windowsadministrator` | The windows administrator user |
| WinPE Samba Server | Linux/Unix Node | `winpesambaserver` |  |




## Files

| Name | Type | Comment |
| ---- | ---- | ------- |
| AD Root CA certificate | Version Controlled Files | This is the root CA certificate from the Windows Ad server.  Used for all SSL security procedures and SSL to the rpm servers |
| Attune Powershell v6.2.4 | Large Archives | https://github.com/PowerShell/PowerShell/releases |
| Attune Release v4.3.13 | Large Archives | This is  EL7, EL8 has a cryptography problem |
| Attune v4 Offline CMake Source | Version Controlled Files |  |
| Attune v4 Offline PostgreSQL Source | Large Archives |  |
| Attune v4 Offline Python PyPIs | Large Archives | https://pypi.org/project/pip/#files<br>https://pypi.org/project/virtualenv/#files<br>https://pypi.org/project/wheel/#files |
| Attune v4 Offline Python Source | Large Archives |  |
| Attune v4 Offline TimescaleDB Source | Version Controlled Files |  |
| Attune VMWare. PowerCLI | Large Archives | This was downloaded with :<br>sudo pwsh -Command "Save-Module -name VMware.PowerCLI -Path /root" |
| Linux: chrony.conf | Version Controlled Files |  |
| oVirt Drivers 09-Jan-2023 | Large Archives |  |
| oVirt Guest Drivers | Version Controlled Files | from c:\program files\virtio-win |
| oVirt Offline Python PyPis | Version Controlled Files |  |
| RHEL8 AD Config Files | Version Controlled Files |  |
| RHEL8 Boot ISO v8.4 | Large Archives |  |
| RHEL8 EPEL iftop | Version Controlled Files | These RPMs were gathered with a CentOS7 VM.<br><br>sudo yum install epel-release<br><br>mkdir pkg_iftop && cd pkg_iftop<br>yum install --downloadonly --downloaddir=. iftop<br>cd ..<br>tar cf pkg_iftop.tar pkg_iftop<br>rm -rf pkg_iftop |
| RHEL8 EPEL nmon | Version Controlled Files | These RPMs were gathered with a CentOS7 VM.<br><br>sudo yum install epel-release<br><br>mkdir pkg_nmon && cd pkg_nmon<br>yum install --downloadonly --downloaddir=. nmon<br>cd ..<br>tar cf pkg_nmon.tar pkg_nmon<br>rm -rf pkg_nmon |
| RHEL8 EPEL p7zip | Version Controlled Files | These RPMs were gathered with a CentOS7 VM.<br><br>sudo yum install epel-release<br><br>mkdir pkg_p7zip && cd pkg_p7zip<br>yum install --downloadonly --downloaddir=. p7zip p7zip-plugins<br>cd ..<br>tar cf pkg_p7zip.tar pkg_p7zip<br>rm -rf pkg_p7zip |
| RHEL8 etc Configs | Version Controlled Files |  |
| RHEL8 Kickstart Config | Version Controlled Files | https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/installation_guide/s1-kickstart2-options |
| SSL Certs from AD | Version Controlled Files |  |
| wimlib | Large Archives |  |
| WinPE 2019 ISO | Large Archives |  |
| WinPE ISO for oVirt 09-Jan-2023 | Large Archives |  |
| WinPE ISO for Windows 10 ESXi | Large Archives |  |
| WinPE ISO for Windows 10 oVirt | Large Archives |  |
| WinPE startnet.cmd for Win10 | Version Controlled Files |  |
| WinPE startnet.cmd for Win2016 | Version Controlled Files |  |
| WinPE startnet.cmd for Win2019 | Version Controlled Files |  |
| WinPE startnet.cmd for Win2022 | Version Controlled Files |  |
| WIN VMWare Drivers | Large Archives | These drivers are compatible with Windows 10 and Windows Server 2016 Std.<br><br>To obtain the files required for this archive, create a new Windows Server 2016 VM, install the VMWare Tools, and zip up the directory C:\Program Files\Common Files\VMware\Drivers |
| WIN Win10 Unattended Config HyperV | Version Controlled Files | Copied from "WIN Win10 Unattended Config HyperV".<br><br>Changed LogonCount from 1 to 88. |
| WIN Win10 Unattended Config oVirt with Drivers | Version Controlled Files |  |
| WIN Win2019 Unattended Config HyperV | Version Controlled Files | This is the same as file archive "WIN Win2019 Unattended Config with Drivers". |
| WIN Win2019 Unattended Config with Drivers | Version Controlled Files |  |






# Contribute to this Project

**The collective power of a community of talented individuals working in
concert delivers not only more ideas, but quicker development and
troubleshooting when issues arise.**

If you’d like to contribute and help improve these projects, please fork our
repository, commit your changes in Attune, push you changes, and create a
pull request.

<img src="https://www.servertribe.com/wp-content/uploads/2023/02/Attune-pull-request-01.png" alt="pull request"/>

---

Please feel free to raise any issues or questions you have.

<img src="https://www.servertribe.com/wp-content/uploads/2023/02/Attune-get-help-02.png" alt="create an issue"/>


---

**Thank you**
